<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'connect.php';
    
    $query = "SELECT * FROM wrestler ORDER BY name";
    $statement = $db->prepare($query);
    $statement->execute();
    $entries = $statement->fetchAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE - Browse Wrestlers</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet"> 
</head>
<body>
	<div id="header">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>
	<ul id="menu">
		<li><a href="index.php">HOME</a></li>
		<li><a href="browseWrestlers.php" class="active">BROWSE WRESTLERS</a></li>

		<?php if(isset($_SERVER['PHP_AUTH_USER']) || isset($_SERVER['PHP_AUTH_PW'])): ?>
			<li><a href="addWrestler.php">ADD AN ENTRY</a></li>
		<?php endif ?>
		<li><a href="">LOGIN</a></li>
	</ul>	

	<div id="content">
		<div id="side_menu">
			<h4>Categories</h4>
			<ul>
				<li>Wrestlers</li>
				<li>Promotions</li>
			</ul>
		</div>


		<div class="wrapper">
			<h3>Browse Wrestlers</h3>
			<div>
				<?php if ($entries == null): ?>
					<p>No Wrestlers in Database</p>
				<?php else: ?>
					<table width="100%">				
						<thead>
							<tr>
								<th width="30%">Name</th>
								<th width="30%">Real Name</th>
								<th>Date Of Birth</th>
								<th>Age</th>
								<th>Nationality</th>
							</tr>						
						</thead>
						<tbody>
							<?php foreach($entries as $entry): ?>
							<tr>
								<td><a href="show.php?id=<?= $entry['wrestler_id'] ?>"><?= $entry['name'] ?></a></td>
								<td><?= $entry['real_name'] ?></td>
								<td><?= $entry['date_of_birth'] ?></td>
								<td><?= floor((time() - strtotime($entry['date_of_birth']))/31556926) ?></td>
								<td><?= $entry['nationality'] ?></td>
							</tr>
							<p><small>
								
							</small></p>
							<div class="entry_content">							
							</div>						
						</div>
							<?php endforeach ?>								
						</tbody>
					</table>
				<?php endif ?>
			</div>
		</div>
	</div>	
</body>
</html>