<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'authenticate.php';
	require 'imageUpload.php';

	if(isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
		header('Location: index.php');
	}

	$name_valid = true;
	$real_name_valid = true;
	$date_of_birth_valid = true;
	$nationality_valid = true;
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

	$id_type = $_GET['type'] . "_id";

    $query = "SELECT * FROM {$_GET['type']} WHERE {$id_type}  = $id";
    $statement = $db->prepare($query);
    $statement->execute();
    $entry = $statement->fetch();

    switch($_GET['type']) {
    	case 'event':
    	case 'wrestler':
    		$query = "SELECT * FROM promotion";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $promotions = $statement->fetchAll();
    		break;

    	case 'wrestlingmatch':
    		$query = "SELECT * FROM wrestler ORDER BY name";
    		$statement = $db->prepare($query);
    		$statement->execute();
    		$wrestlers = $statement->fetchAll();

    		$query = "SELECT * FROM event";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $events = $statement->fetchAll();     

			$query = "SELECT wrestlingmatch.wrestlingmatch_id, event_date, event.name, duration, finish, titles, type,
    			GROUP_CONCAT(DISTINCT CASE WHEN winner = 'y' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR ',') AS 'winner',
    			GROUP_CONCAT(DISTINCT CASE WHEN winner = 'n' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR ',') AS 'loser'    	
    			FROM wrestlingmatch
    			JOIN event ON event.event_id = wrestlingmatch.event_id
    			JOIN matchwrestler ON matchwrestler.wrestlingmatch_id = wrestlingmatch.wrestlingmatch_id
    			JOIN wrestler ON wrestler.wrestler_id = matchwrestler.wrestler_id
    			
		    	AND wrestlingmatch.wrestlingmatch_id = $id
		    	GROUP BY matchwrestler.wrestlingmatch_id";

		    $statement = $db->prepare($query);
		    $statement->execute();
		    $entry = $statement->fetch();

	    	break;
    }

    $query = "SELECT image_id, filename FROM image WHERE wrestler_id = $id";
    $statement = $db->prepare($query);
    $statement->execute();
    $images = $statement->fetchAll();	    

	if(isset($_POST['submit'])) {
		switch ($_GET['type']) {
			case 'wrestler':
			    $name  = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $real_name  = filter_input(INPUT_POST, 'real_name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $date_of_birth = $_POST['date_of_birth'];
			    $nationality  = filter_input(INPUT_POST, 'nationality', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $gender = $_POST['gender'];
			   	$promotion  =  $_POST['promotion'];
			    
			    $query = "UPDATE wrestler SET name = :name, real_name = :real_name, date_of_birth = :date_of_birth, nationality = :nationality, gender = :gender, promotion_id = :promotion WHERE wrestler_id = :id";
			    $statement = $db->prepare($query);
			    $statement->bindValue(':name', $name);
			    $statement->bindValue(':real_name', $real_name);
			    $statement->bindValue(':date_of_birth', $date_of_birth);
			    $statement->bindValue(':nationality', $nationality);
			    $statement->bindValue(':gender', $gender);
			    $statement->bindValue(':promotion', $promotion);
			    $statement->bindValue(':id', $id, PDO::PARAM_INT);
			    $successful = $statement->execute();

			    if(isset($_POST['images'])) {
				    foreach ($_POST['images'] as $image) {
				    	$query = "DELETE FROM image WHERE image_id  = :id";
						$statement = $db->prepare($query);
						$statement->bindValue(':id', $image, PDO::PARAM_INT);
						$statement->execute();
				    }
				}

			    if($successful) {
			    	image_upload($id);			    	
			    }
			    
				break;

			case 'wrestlingmatch':
				$duration = ((int)$_POST['minutes'] * 60) + $_POST['seconds'];				
				$type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
				$finish = filter_input(INPUT_POST, 'finish', FILTER_SANITIZE_FULL_SPECIAL_CHARS);	
				$titles = filter_input(INPUT_POST, 'titles', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
				$event_id = $_POST['event'];

			    $query = "UPDATE wrestlingmatch SET duration = :duration, type = :type, finish = :finish, titles = :titles, event_id = :event_id WHERE wrestlingmatch_id = :id";
			    $statement = $db->prepare($query);
			    $statement->bindValue(':duration', $duration);
			    $statement->bindValue(':type', $type);
			    $statement->bindValue(':finish', $finish);
			    $statement->bindValue(':titles', $titles);
			    $statement->bindValue(':event_id', $event_id);
			    $statement->bindValue(':id', $id, PDO::PARAM_INT);		    
			    $successful = $statement->execute();

			    if($successful) {
			    	for ($i=1; $i <= $_SESSION['winner_count'] && $successful; $i++) {			    		
						$wrestler_id = $_POST['winner' . $i];
				    	$winner = 'y';
				    	$original_winner = $_SESSION['original_winner' . $i];
			   			$query = "UPDATE matchwrestler SET wrestler_id = :wrestler_id
			    			WHERE wrestlingmatch_id = :wrestlingmatch_id 
			    			AND winner = 'y'
			    			AND wrestler_id = :original_winner";
				    	$statement = $db->prepare($query);
				    	$statement->bindValue(':wrestler_id', $wrestler_id);
				    	$statement->bindValue(':wrestlingmatch_id', $id);
				    	$statement->bindValue(':original_winner', $original_winner);
				    	$successful =$statement->execute();
			    	}			    

				    for ($i=1; $i <= $_SESSION['loser_count'] && $successful; $i++) { 
					    $wrestler_id = $_POST['loser' . $i];
					    $winner = 'n';
					    $original_loser = $_SESSION['original_loser' . $i];
			   			$query = "UPDATE matchwrestler SET wrestler_id = :wrestler_id
			    			WHERE wrestlingmatch_id = :wrestlingmatch_id 
			    			AND winner = 'n'
			    			AND wrestler_id = :original_loser";
					    $statement = $db->prepare($query);
					    $statement->bindValue(':wrestler_id', $wrestler_id);
					    $statement->bindValue(':wrestlingmatch_id', $id);
					    $statement->bindValue(':original_loser', $original_loser);
					    $successful = $statement->execute();
				    }			    	
			    }			    

				break;

			case 'event':
			    $name  = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $event_date = $_POST['event_date'];
			    $venue  = filter_input(INPUT_POST, 'venue', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $promotion  = $_POST['promotion'];
			    $query = "UPDATE event SET name = :name, event_date = :event_date, venue = :venue, promotion_id = :promotion 
			    			WHERE event_id = :id";
			    
			    $statement = $db->prepare($query);
			    $statement->bindValue(':name', $name);
			    $statement->bindValue(':event_date', $event_date);
			    $statement->bindValue(':venue', $venue);
			    $statement->bindValue(':promotion', $promotion);
			    $statement->bindValue(':id', $id, PDO::PARAM_INT);
			    $successful = $statement->execute();

				break;	

			case 'promotion':
			    $name  = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $location  = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    
			    $query = "UPDATE promotion SET name = :name, location = :location WHERE promotion_id = :id";
			    $statement = $db->prepare($query);
			    $statement->bindValue(':name', $name);
			    $statement->bindValue(':location', $location);
			    $statement->bindValue(':id', $id, PDO::PARAM_INT);
			    $successful = $statement->execute();

				break;			
		}

		header("Location: browse.php?edit=Edit");
	    exit(); 
	
    }

    if(isset($_POST['delete'])) {
		$query = "DELETE FROM {$_GET['type']} WHERE {$id_type}  = $id";
		$statement = $db->prepare($query);
		$statement->bindValue(':id', $id, PDO::PARAM_INT);
		$statement->execute();

		header("Location: browse.php?edit=Delete");
	    exit(); 
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE - Edit Wrestler</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Heebo:900&display=swap" rel="stylesheet"> 
</head>
<body>	
	<div id="header">
		<img src="images/logo.png">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>		

	<ul id="menu">
		<li><a href="index.php">HOME</a></li>
		<li><a href="browse.php">BROWSE DATABASE</a></li>
		<li><a href="search.php">SEARCH DATABASE</a></li>
		<li><a href="addEntry.php">ADD AN ENTRY</a></li>
	</ul>

	<form method="post" id="entry_form" enctype="multipart/form-data">

		<div id="side_menu">
			<h3>CATEGORY</h3>
			<ul>
				<li><a href="browse.php?type=wrestler&sort=wrestler_id">WRESTLERS</a></li>
				<li><a href="browse.php?type=match&sort=wrestlingmatch_id">MATCHES</a></li>
				<li><a href="browse.php?type=event&sort=event_id">EVENT</a></li>
				<li><a href="browse.php?type=promotion&sort=promotion_id">PROMOTIONS</a></li>
			</ul>
		</div>

		<div class="wrapper">

			<?php switch ($_GET['type']): 
				case 'wrestler': ?>
					<h3>EDIT WRESTLER</h3>
					<label for="title">NAME</label>
					<input type="text" name="name" class="info" required value="<?= $entry['name'] ?>" >

					<label for=real_name>REAL NAME</label>
					<input type="text" name="real_name" class="info"  required value="<?= $entry['real_name'] ?>">

					<label for=real_name>DATE OF BIRTH</label>
					<input type="date" name="date_of_birth" id="date_of_birth" required value="<?= $entry['date_of_birth'] ?>">	

					<label for=real_name>NATIONALITY</label>
					<input type="text" name="nationality" class="info"  required value="<?= $entry['nationality'] ?>">

					<label for=gender>GENDER</label>
					<select name="gender" required >
						<option value="m" <?php echo ($entry['gender'] == 'm')?"selected":""; ?>>Male</option>
						<option value="f" <?php echo ($entry['gender'] == 'f')?"selected":""; ?>>Female</option>
						<option value="o" <?php echo ($entry['gender'] == 'o')?"selected":""; ?>>Other</option>
					</select>

					<label for=promotion>PROMOTION</label>
					<select name="promotion" required >
						<?php foreach($promotions as $promotion): ?>
							<option value="<?=$promotion['promotion_id'] ?>" <?php echo ($entry['promotion_id'] == $promotion['promotion_id'])?"selected":""; ?>><?=$promotion['name'] ?></option>
						<?php endforeach ?>
					</select>						

					<h3>DELETE IMAGES</h3>
					<label>CHECK IMAGE TO DELETE:</label>
					<div class="images">	
						<?php if($images == null): ?>						
								<p>No images uploaded.</p>						
						<?php else: ?>
							<table>
								<tbody>
									<tr>
										<?php foreach($images as $image): ?>
											<td><input type="checkbox" name="images[]" value="<?= $image['image_id'] ?>"></td>
											<td><a href="uploads/<?= $image['filename'] ?>"><img src="uploads/<?= substr_replace($image['filename'], '_thumbnail', strpos($image['filename'], '.'), 0)?>"></a></td>											
										<?php endforeach ?>
									</tr>
								</tbody>
							</table>

						<?php endif ?>
					</div>	

					<h3>ADD AN IMAGE</h3>
					<label for="image">ENTER IMAGE FILENAME:</label>
					<input type="file" name="image" id="image">

				<?php break; ?>

				<?php case 'wrestlingmatch': ?>
					<h3>EDIT MATCH</h3>
					<label for="participants">PARTICIPANTS</label>
					<div id="participants">
						<div>
							<?php $winner_tok = strtok($entry['winner'], ","); ?>
							<?php $_SESSION['winner_count'] = 0; ?>	
							<?php while ($winner_tok !== false): ?>
								<?php $_SESSION['winner_count']++; ?>								
								<select name="winner<?= $_SESSION['winner_count'] ?>" required >
									<?php foreach($wrestlers as $wrestler): ?>
										<option value="<?=$wrestler['wrestler_id'] ?>" <?php echo ($wrestler['name'] == $winner_tok)? ("selected"):""; ?>><?=$wrestler['name'] ?></option>
										<?php if($wrestler['name'] == $winner_tok): ?>
											<?php $_SESSION['original_winner' . $_SESSION['winner_count']] = $wrestler['wrestler_id']?>
										<?php endif ?>
									<?php endforeach ?>
								</select>
								<?php $winner_tok = strtok(","); ?></br>
							<?php endwhile ?>
						</div>

						<select name="finish" required >
							<option value="">Result</option>
							<option value="Def. (Pin)" <?php echo ($entry['finish'] == "Def. (Pin)")?"selected":""; ?>>Defeated (via Pinfall)</option>
							<option value="Def. (Submission)" <?php echo ($entry['finish'] == "Def. (Submission)")?"selected":""; ?>>Defeated (via Submission)</option>
							<option value="Def. (DQ)" <?php echo ($entry['finish'] == "Def. (DQ)")?"selected":""; ?>>Defeated (via Disqualification)</option>
							<option value="No Contest" <?php echo ($entry['finish'] == "No Contest")?"selected":""; ?>>No Contest</option>											
						</select>	

						<div>
							<?php $loser_tok = strtok($entry['loser'], ","); ?>
							<?php $_SESSION['loser_count'] = 0; ?>	
							<?php while ($loser_tok !== false): ?>
								<?php $_SESSION['loser_count']++; ?>						
								<select name="loser<?= $_SESSION['loser_count'] ?>" required >
									<?php foreach($wrestlers as $wrestler): ?>
										<option value="<?=$wrestler['wrestler_id'] ?>" <?php echo ($wrestler['name'] == $loser_tok)? ("selected"):""; ?>><?=$wrestler['name'] ?></option>
										<?php if($wrestler['name'] == $loser_tok): ?>
											
											<?php $_SESSION['original_loser' . $_SESSION['loser_count']] = $wrestler['wrestler_id']?>
										<?php endif ?>
									<?php endforeach ?>
								</select>
								<?php $loser_tok = strtok(","); ?></br>
							<?php endwhile ?>
						</div>							
					</div>
					<label for="duration">DURATION</label>
					<div id="duration">
						<label for="minutes">MINUTES:</label>					
						<input type="text" name="minutes" class="duration" value="<?= (int)($entry['duration']/60) ?>">
						<label for="seconds">SECONDS:</label>
						<input type="number" name="seconds" class="duration" value="<?= (int)($entry['duration']%60) ?>">
					</div>

					<label for="type">TYPE</label>
					<input type="text" name="type" class="info" value="<?= $entry['type'] ?>">

					<label for="titles">TITLES</label>
					<input type="text" name="titles" class="info" value="<?= $entry['titles'] ?>" readonly>				

					<label for="event">EVENT</label>
					<select name="event" required >
						<option value="">Please select...</option>
						<option value="none">None</option>						
						<?php foreach($events as $event): ?>
							<option value="<?=$event['event_id'] ?>" <?php echo ($event['name'] == $entry['name'])?"selected":""; ?>><?=$event['name'] ?></option>
						<?php endforeach ?>
					</select>					
				<?php break; ?>

				<?php case 'event': ?>
					<h3>EDIT EVENT</h3>
					<label for="name">NAME</label>
					<input type="text" name="name" class="info" required value="<?= $entry['name'] ?>">

					<label for="event_date">DATE</label>
					<input type="date" name="event_date" id="event_date" required value="<?= $entry['event_date'] ?>">

					<label for="venue">VENUE</label>
					<input type="text" name="venue" class="info" required value="<?= $entry['venue'] ?>">					

					<label for=promotion>PROMOTION</label>
					<select name="promotion" required >
						<option value="none">None</option>
						<?php foreach($promotions as $promotion): ?>
							<option value="<?=$promotion['promotion_id'] ?>" <?php echo ($entry['promotion_id'] == $promotion['promotion_id'])?"selected":""; ?>><?=$promotion['name'] ?></option>
						<?php endforeach ?>
					</select>	
				<?php break; ?>

				<?php case 'promotion': ?>
					<h3>EDIT PROMOTION</h3>
					<label for="name">Name</label>
					<input type="text" name="name" class="info" required value="<?= $entry['name'] ?>">

					<label for="name">Location</label>
					<input type="text" name="location" class="info" required value="<?= $entry['location'] ?>">
				<?php break; ?>
			<?php endswitch; ?>	

			<input type="submit" name="submit" class="submitbutton" value="UPDATE">
			<input type="submit" name="delete" class="deletebutton" value="DELETE">
		</div>

		<div class="account">
			<h3>ACCOUNT MENU</h3>
			<h4>Hi <?= $_SESSION['username'] ?>!</h4>
			<ul>
				<li><a href="logout.php">LOGOUT</a></li>
			</ul>
		</div>	
	</form>

		
</body>
</html>