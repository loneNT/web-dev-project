<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'connect.php';
	require 'authenticate.php';

	$id_type = $_GET['type'] . "_id";

    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

    $query = "SELECT * FROM {$_GET['type']} WHERE {$id_type} = $id";
    $statement = $db->prepare($query);
    $statement->bindValue(':id', $id);
    $statement->execute();    
    $entry = $statement->fetch();

	switch ($_GET['type']) {
		case 'wrestler':  
			$query = "SELECT * FROM promotion";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $promotions = $statement->fetchAll();

		    $query = "SELECT * FROM matchwrestler 
		    	INNER JOIN wrestlingmatch ON matchwrestler.wrestlingmatch_id = wrestlingmatch.wrestlingmatch_id
		    	INNER JOIN event ON wrestlingmatch.event_id = event.event_id
		    	WHERE matchwrestler.wrestler_id = $id
		    	ORDER BY event.event_date DESC";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $matches = $statement->fetchAll();
	    
			$query = "SELECT wrestler.name, wrestler.wrestler_id FROM wrestler 
				JOIN matchwrestler ON wrestler.wrestler_id = matchwrestler.wrestler_id
				WHERE matchwrestler.winner = 'y' AND matchwrestler.wrestlingmatch_id IN (SELECT wrestlingmatch_id FROM matchwrestler WHERE wrestler_id = $id)
				ORDER BY matchwrestler.wrestlingmatch_id";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $winners = $statement->fetchAll();

			$query = "SELECT wrestler.name, wrestler.wrestler_id FROM wrestler 
				JOIN matchwrestler ON wrestler.wrestler_id = matchwrestler.wrestler_id
				WHERE matchwrestler.winner = 'n' AND matchwrestler.wrestlingmatch_id IN (SELECT wrestlingmatch_id FROM matchwrestler WHERE wrestler_id = $id)
				ORDER BY matchwrestler.wrestlingmatch_id";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $losers = $statement->fetchAll();

		    $query = "SELECT filename FROM image WHERE wrestler_id = $id";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $images = $statement->fetchAll();		    

		    break;

		case 'wrestlingmatch':
			$query = "SELECT wrestlingmatch.wrestlingmatch_id, event_date, event.name, duration, finish, titles, type,
    			GROUP_CONCAT(DISTINCT CASE WHEN winner = 'y' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR '\n') AS 'winner',
    			GROUP_CONCAT(DISTINCT CASE WHEN winner = 'n' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR '\n') AS 'loser'    	
    			FROM wrestlingmatch
    			JOIN event ON event.event_id = wrestlingmatch.event_id
    			JOIN matchwrestler ON matchwrestler.wrestlingmatch_id = wrestlingmatch.wrestlingmatch_id
    			JOIN wrestler ON wrestler.wrestler_id = matchwrestler.wrestler_id
    			
		    	AND wrestlingmatch.wrestlingmatch_id = $id
		    	GROUP BY matchwrestler.wrestlingmatch_id";

		    $statement = $db->prepare($query);
		    $statement->execute();
		    $entry = $statement->fetch();

		    $query = "SELECT rating.date, rating.description, rating.star_rating, user.username 
		    	FROM rating, user 
		    	WHERE rating.user_id = user.user_id AND wrestlingmatch_id = $id
		    	ORDER BY rating.date DESC";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $ratings = $statement->fetchAll();
			break;

		case 'event':
			$query = "SELECT wrestlingmatch.wrestlingmatch_id, event_date, event.name, duration, finish, titles, type,
    			GROUP_CONCAT(DISTINCT CASE WHEN winner = 'y' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR ', ') AS 'winner',
    			GROUP_CONCAT(DISTINCT CASE WHEN winner = 'n' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR ', ') AS 'loser'    	
    			FROM wrestlingmatch
    			JOIN event ON event.event_id = wrestlingmatch.event_id
    			JOIN matchwrestler ON matchwrestler.wrestlingmatch_id = wrestlingmatch.wrestlingmatch_id
    			JOIN wrestler ON wrestler.wrestler_id = matchwrestler.wrestler_id
    			
		    	AND event.event_id = $id
		    	GROUP BY matchwrestler.wrestlingmatch_id";

		    $statement = $db->prepare($query);
		    $statement->execute();
		    $matches = $statement->fetchAll();
			
		    break;

		case 'promotion':
			$query = "SELECT wrestler_id, name FROM wrestler WHERE promotion_id = $id ORDER BY {$_GET['sort']}";
		    $statement = $db->prepare($query);
		    $statement->execute();
		    $roster = $statement->fetchAll();		
			break;
	}         
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE - Entry Details</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Heebo:900&display=swap" rel="stylesheet">   
</head>
<body>
	
	<div id="header">
		<img src="images/logo.png">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>		
	
	<ul id="menu">
		<li><a href="index.php">HOME</a></li>
		<li><a href="browse.php">BROWSE DATABASE</a></li>
		<li><a href="search.php">SEARCH DATABASE</a></li>
		<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
			<?php if ($_SESSION['user_type'] == 'admin'): ?>
				<li><a href="addEntry.php">ADD AN ENTRY</a></li>
			<?php endif ?>
		<?php endif ?>
	</ul>		
			
	<div id="content">
		<div id="side_menu">
			<h3>CATEGORY</h3>
			<ul>
				<li><a href="browse.php?type=wrestler&sort=wrestler_id">WRESTLERS</a></li>
				<li><a href="browse.php?type=match&sort=wrestlingmatch_id">MATCHES</a></li>
				<li><a href="browse.php?type=event&sort=event_id">EVENT</a></li>
				<li><a href="browse.php?type=promotion&sort=promotion_id">PROMOTIONS</a></li>
			</ul>
		</div>

		<?php switch ($_GET['type']): 
		case 'wrestler': ?>
		<div class="wrapper">
			<h3>WRESTLER DETAILS</h3>
			<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
				<?php if ($_SESSION['user_type'] == 'admin'): ?>	
					<small><a href="edit.php?type=<?= $_GET['type'] ?>&id=<?= $id ?>">Edit Entry</a></small></br></br>
				<?php endif ?>					
			<?php endif ?>	
			<label for="title">NAME</label>
			<input type="text" name="name" class="info" value="<?= $entry['name'] ?>" readonly>

			<label for=real_name>REAL NAME</label>
			<input type="text" name="real_name" class="info" value="<?= $entry['real_name'] ?>"  readonly>

			<label for=date_of_birth>DATE OF BIRTH</label>
			<input type="date" name="date_of_birth" id="date_of_birth" value="<?= $entry['date_of_birth'] ?>" readonly>

			<label for=nationality>NATIONALITY</label>
			<input type="text" name="nationality" class="info" value="<?= $entry['nationality'] ?>" readonly>

			<label for=gender>GENDER</label>
			<select name="gender" disabled>
				<option value="m" <?php echo ($entry['gender'] == 'm')?"selected":""; ?>>Male</option>
				<option value="f" <?php echo ($entry['gender'] == 'f')?"selected":""; ?>>Female</option>
				<option value="o" <?php echo ($entry['gender'] == 'o')?"selected":""; ?>>Other</option>
			</select>

			<label for=promotion>PROMOTION</label>
			<select name="promotion" disabled>
				<option value="none">None</option>
				<?php foreach($promotions as $promotion): ?>
					<option value="<?=$promotion['promotion_id'] ?>" <?php echo ($entry['promotion_id'] == $promotion['promotion_id'])?"selected":""; ?>><?=$promotion['name'] ?></option>
				<?php endforeach ?>
			</select>

			<label>IMAGES</label>
			<div class="images">	
				<?php if($images == null): ?>						
						<p>No images uploaded.</p>						
				<?php else: ?>
					<?php foreach($images as $image): ?>
						<a href="uploads/<?= $image['filename'] ?>"><img src="uploads/<?= substr_replace($image['filename'], '_thumbnail', strpos($image['filename'], '.'), 0)?>"></a>
					<?php endforeach ?>
				<?php endif ?>
			</div>	

			<label>MATCH RECORD</label>
			<div class="datatable">
				<?php if($matches == null): ?>
					<p>No matches found!</p>
				<?php else: ?>
					<table width="100%">			
						<thead>									
							<tr>
								<th width="10%">DATE</th>
								<th>EVENT</th>										
							</tr>												
						</thead>
						<tbody>
							<?php for ($i=0; $i < count($matches); $i++): ?>
								<tr onclick="location.href='show.php?type=wrestlingmatch&id=<?= $matches[$i]['wrestlingmatch_id'] ?>'">
									<td><?= $matches[$i]['event_date'] ?></td>
									<td><?= $matches[$i]['name'] ?></td>
									<td><?= $winners[$i]['name'] ?></td>
									<td><?= $matches[$i]['finish'] ?></td>
									<td><?= $losers[$i]['name'] ?></td>
								</tr>	
							<?php endfor ?>								
						</tbody>
					</table>				
				<?php endif ?>
			</div>						
		</div>
		<?php break; ?>

		<?php case 'wrestlingmatch': ?>
			<div class="wrapper">
				<h3>MATCH DETAILS</h3>
				<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
					<?php if ($_SESSION['user_type'] == 'admin'): ?>	
						<small><a href="edit.php?type=<?= $_GET['type'] ?>&id=<?= $id ?>">Edit Entry</a></small></br></br>
					<?php endif ?>					
				<?php endif ?>		
				<label for="participants">PARTICIPANTS</label>
				<div id='participants'>
					<textarea readonly><?= $entry['winner']; ?></textarea>
					<input type="text" value="<?= $entry['finish'] ?>" readonly>
					<textarea readonly><?= $entry['loser'] ?></textarea>
				</div>

				<label for="duration">DURATION</label>
				<div id="duration">
					<label for="minutes">MINUTES:</label>					
					<input type="text" name="minutes" class="duration" value="<?= (int)($entry['duration']/60) ?>" readonly>
					<label for="seconds">SECONDS:</label>
					<input type="number" name="seconds" class="duration" value="<?= (int)($entry['duration']%60) ?>" readonly>
				</div>

				<label for="type">TYPE</label>
				<input type="text" name="type" class="info" value="<?= $entry['type'] ?>" readonly>

				<label for="titles">TITLES</label>
				<input type="text" name="titles" class="info" value="<?= $entry['titles'] ?>" readonly>													

				<label for="event">EVENT</label>
				<input type="text" name="event" class="info" value="<?= $entry['name'] ?>" readonly>

				<label>USER REVIEWS</label>		
				<div class="datatable">
					<?php if($ratings == null): ?>
						<p>No ratings found!</p>
					<?php else: ?>
						<table width="30%">
							<thead>
								<tr>
									<th width="15%">DATE</th>
									<th width="10%">STAR RATING</th>
									<th width="10%">USER</th>	
									<th width="30%">DESCRIPTION</th>
								</tr>						
							</thead>
							<tbody>
								<?php foreach($ratings as $rating): ?>
								<tr>
									<td><?= $rating['date'] ?></td>
									<td><?= $rating['star_rating'] ?></td>
									<td><?= $rating['username'] ?></td>
									<?php if(strlen($rating['description']) > 200): ?>
										<p><?= substr($rating['description'], 0, 200); ?></p>
									<?php else: ?>
										<td><?= $rating['description'] ?></td>
									<?php endif ?>									
								</tr>				
							</div>
								<?php endforeach ?>								
							</tbody>
						</table>
					<?php endif ?>	
				</div>
				<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
					<input type="submit" value="LEAVE A RATING" onclick="window.location='rateMatch.php?id=<?= $id ?>';">
				<?php endif ?>									
			</div>
		<?php break; ?>	

		<?php case 'event': ?>
			<div class="wrapper">
				<h3>EVENT DETAILS</h3>
				<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
					<?php if ($_SESSION['user_type'] == 'admin'): ?>	
						<small><a href="edit.php?type=<?= $_GET['type'] ?>&id=<?= $id ?>">Edit Entry</a></small></br></br>
					<?php endif ?>					
				<?php endif ?>						
				<label for="name">NAME</label>
				<input type="text" name="name" class="info" value="<?= $entry['name'] ?>" readonly>

				<label for=event_date>EVENT DATE</label>
				<input type="date" name="event_date" id="event_date" value="<?= $entry['event_date'] ?>" readonly>

				<label for="venue">VENUE</label>
				<input type="text" name="venue" class="info" value="<?= $entry['venue'] ?>" readonly>

				<label for="promotion">PROMOTION</label>
				<input type="text" name="venue" class="info" value="<?= $matches[0]['name'] ?>" readonly>

				<label>MATCHES</label>
				<div class="datatable">
					<?php if($matches == null): ?>
						<p>No matches found!</p>
					<?php else: ?>
						<table>
							<tbody>
								<?php foreach ($matches as $match): ?>
									<tr onclick="location.href='show.php?type=wrestlingmatch&id=<?= $match['wrestlingmatch_id'] ?>'">
										<td><?= $match['winner'] ?></td>
										<td><?= $match['finish'] ?></td>
										<td><?= $match['loser'] ?></td>
									</tr>	
								<?php endforeach ?>								
							</tbody>
						</table>				
					<?php endif ?>
				</div>											
		</div>
		<?php break; ?>

		<?php case 'promotion': ?>
			<div class="wrapper">
				<h3>PROMOTION DETAILS</h3>
				<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
					<?php if ($_SESSION['user_type'] == 'admin'): ?>	
						<small><a href="edit.php?type=<?= $_GET['type'] ?>&id=<?= $id ?>">Edit Entry</a></small></br></br>
					<?php endif ?>					
				<?php endif ?>						
				<label for="name">NAME</label>
				<input type="text" name="name" class="info" value="<?= $entry['name'] ?>" readonly>

				<label for="name">LOCATION</label>
				<input type="text" name="location" class="info" value="<?= $entry['location'] ?>" readonly>			
			
				<label>CURRENT ROSTER</label>
				<div class="datatable">
					<?php if($roster == null): ?>
						<p>Roster is empty!</p>
					<?php else: ?>
						<table width="30%">
							<thead>
								<tr>
									<th width="30%">NAME</tr>						
							</thead>
							<tbody>
								<?php foreach($roster as $entry): ?>
								<tr onclick="location.href='show.php?type=wrestler&id=<?= $entry['wrestler_id'] ?>'">
									<td><?= $entry['name'] ?></a></td>
								</tr>				
							</div>
								<?php endforeach ?>								
							</tbody>
						</table>
					<?php endif ?>	
				</div>
			</div>
		<?php break; ?>
		<?php endswitch; ?>

		<?php if($_SESSION['loggedin'] == false): ?>		
			<div class="login">
				<h3>MEMBERS LOGIN</h3>
				<form method = "post" action="" id="login">
					<input type="text" name="username" placeholder="Username" required class="login_input"/>
					<input type="password" name="password" placeholder="Password" required class="login_input" />
					<input type="submit" value="LET ME IN!" class="login_input"/>
				</form>
				<a href="createAccount.php">Create An Account</a>	
			</div>
		<?php else: ?>
			<div class="account">
				<h3>ACCOUNT MENU</h3>
				<h4>Hi <?= $_SESSION['username'] ?>!</h4>
				<ul>
					<li><a href="logout.php">LOGOUT</a></li>
				</ul>
			</div>			
		<?php endif ?>			
	</div>
</body>
</html>