<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'connect.php';
	require 'authenticate.php';

	$successful = false;

	if(isset($_POST['submit'])) {	

		if($_POST['password'] == $_POST['re-password']) {

		    $username  = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		    $password  = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		    $email = $_POST['email'];

		    $query = "INSERT INTO user (user_id, username, password, email, type) 
		    			VALUES (NULL, :username, :password, :email, 'u')";
		    
		    $statement = $db->prepare($query);
		    $statement->bindValue(':username', $username);
		    $statement->bindValue(':password', $password);
		    $statement->bindValue(':email', $email);
		    
		    $successful = $statement->execute();
		}
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE - Create Account</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Heebo:900&display=swap" rel="stylesheet"> 
</head>
<body>
	
	<div id="header">
		<img src="images/logo.png">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>	

	<ul id="menu">
		<li><a href="index.php" class="active">HOME</a></li>
		<li><a href="browse.php">BROWSE DATABASE</a></li>
		<li><a href="search.php">SEARCH DATABASE</a></li>
		<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
			<li><a href="rateMatch.php">RATE A MATCH</a></li>
			<?php if ($_SESSION['user_type'] == 'admin'): ?>
				<li><a href="addEntry.php">ADD AN ENTRY</a></li>
			<?php endif ?>
		<?php endif ?>
	</ul>

	<div id="content">

		<div class="wrapper">

			<?php if(!$successful): ?>
				<h3>CREATE ACCOUNT</h3>
				<form method="post" id="createAccount">
					<label for="username">USERNAME</label>
					<input type="text" name="username" value="<?php echo isset($_POST['username']) ? $_POST['username'] : '' ?>" required />

					<label for="email">E-MAIL</label>
					<input type="email" name="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>" required />				

					<label for="password">PASSWORD</label>
					<input type="password" name="password" required />

					<label for="re-password">RE-PASSWORD</label>
					<input type="password" name="re-password" required />

					<?php if(isset($_POST['password']) && isset($_POST['re-password']) && $_POST['password'] != $_POST['re-password']): ?>
						<h4>PASSWORDS DO NOT MATCH! PLEASE RE-ENTER.</h4>
					<?php endif ?>

					<input type="submit" name="submit" value="CREATE ACCOUNT"/>				
				</form>

			<?php else: ?>
				<h3>ACCOUNT SUCCESSFULLY CREATED!</h3>
				<h4>Welcome <?= $_POST['username'] ?></h4>
				</br>
				<a href="index.php">Back to the main page</a>
			<?php endif ?>


		</div>				
	</div>
</body>
</html>