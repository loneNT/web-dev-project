<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'connect.php';
	require 'authenticate.php';

	$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

	if(isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
		header('Location: index.php');
	}

	$query = "SELECT wrestlingmatch.wrestlingmatch_id, event_date, event.name, duration, finish, titles, type,
		GROUP_CONCAT(DISTINCT CASE WHEN winner = 'y' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR ', ') AS 'winner',
		GROUP_CONCAT(DISTINCT CASE WHEN winner = 'n' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR ', ') AS 'loser'    	
		FROM wrestlingmatch
		JOIN event ON event.event_id = wrestlingmatch.event_id
		JOIN matchwrestler ON matchwrestler.wrestlingmatch_id = wrestlingmatch.wrestlingmatch_id
		JOIN wrestler ON wrestler.wrestler_id = matchwrestler.wrestler_id    			
    	WHERE wrestlingmatch.wrestlingmatch_id = $id
    	GROUP BY matchwrestler.wrestlingmatch_id";

    $statement = $db->prepare($query);
    $statement->execute();
    $match = $statement->fetch();

    if(isset($_POST['submit'])) {
    	$star_rating  = $_POST['star_rating'];
    	$description  = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    	$user_id = $_SESSION['user_id'];

	    $query = "INSERT INTO rating (rating_id, star_rating, description, user_id, wrestlingmatch_id) 
	    			VALUES (NULL, :star_rating, :description, :user_id, :wrestlingmatch_id)";
	    $statement = $db->prepare($query);
	    $statement->bindValue(':star_rating', $star_rating);
	    $statement->bindValue(':description', $description);
	    $statement->bindValue(':user_id', $user_id);
	    $statement->bindValue(':wrestlingmatch_id', $id);
	    
	    $successful = $statement->execute();	    			   	
    }	
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Heebo:900&display=swap" rel="stylesheet"> 
</head>
<body>
	
	<div id="header">
		<img src="images/logo.png">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>	

	<ul id="menu">
		<li><a href="index.php">HOME</a></li>
		<li><a href="browse.php">BROWSE DATABASE</a></li>
		<li><a href="search.php">SEARCH DATABASE</a></li>
		<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
			<?php if ($_SESSION['user_type'] == 'admin'): ?>
				<li><a href="addEntry.php">ADD AN ENTRY</a></li>
			<?php endif ?>
		<?php endif ?>
	</ul>


	<form method="post" id="entry_form">
		<div class="wrapper">

			<?php if (isset($successful)): ?>
				<?php if($successful): ?>
						<h3>Successfully added rating!</h3>
						</br>							
						<a href="show.php?type=wrestlingmatch&id=<?= $id ?>">Return to match page.</a>
				<?php else: ?>
						<h3>Something went wrong...</h3>
						<p>Please try again.</p>
				<?php endif ?>	

			<?php else: ?>	
				<h3>RATE A MATCH</h3>

				<label>MATCH TO RATE</label>
				<p><?= $match['event_date'] ?> - <?= $match['name'] ?></p>
				<p><?= $match['winner'] ?> VS <?= $match['loser'] ?></p>			
				
				<label for="star_rating">RATING (BETWEEN 0 AND 5)</label>
				<input type="number" name="star_rating" min="0" max="5" step=".25"class="rating" required>

				<label for="description">DESCRIPTION</label>
				<textarea name="description" rows="10" cols="90" required>Your views on the match...</textarea>		
			
				<input type="submit" name="submit" value="SUBMIT" class="submitbutton">
			<?php endif ?>		
		</div>
	

		<div class="account">
			<h3>ACCOUNT MENU</h3>
			<h4>Hi <?= $_SESSION['username'] ?>!</h4>
			<ul>
				<li><a href="logout.php">LOGOUT</a></li>
			</ul>
		</div>
	</form>	
</body>
</html>