

function change_entry(entry_type) {
	var entry = document.getElementById("wrapper");

	entry.innerHTML = "";
	
	alert("!");

	switch(entry_type) {
		case "wrestler":
			var gender = ["Male", "Female", "Other"];

			createInput("text", "name", "");
			createInput("text", "real_name", "");
			createInput("date", "date_of_birth", "");
			createInput("text", "nationality", "");
			createSelect("gender", gender);
			break;
		case "promotion":
			createInput("text", "name", "");
			createInput("text", "location", "");
			break;	

	}

	var input = document.createElement("input");
	input.type = "submit";
	input.name = "submit";
	input.id = "submit";
	input.value = "Submit";
	entry.appendChild(input);	
}

function createInput(input_type, input_name, value) {
	var entry = document.getElementById("wrapper");
	createLabel(input_name);

	var input = document.createElement("input");
	input.type = input_type;
	input.name = input_name;
	input.id = input_name;
	input.value = value;
	entry.appendChild(input);	
}

function createSelect(input_name, options) {
	var entry = document.getElementById("wrapper");
	createLabel(input_name);

	var select = document.createElement("select");
	select.name = input_name;
	select.id = input_name;
	for (var i = 0; i < options.length; i++) {
		var option = document.createElement("option");
		option.value = options[i].charAt(0).toLowerCase();
		option.innerHTML = options[i];
		select.appendChild(option);
	}

	entry.appendChild(select);		
}

function createLabel(input_name) {
	var entry = document.getElementById("wrapper");
	var label = document.createElement("label");
	label.for = input_name;
	label.innerHTML = capitalize(input_name.replace(/_/g, " "));
	entry.appendChild(label);	
}

function capitalize(string) {
  string = string.toLowerCase().split(' ');
  for (var i = 0; i < string.length; i++) {
    string[i] = string[i].charAt(0).toUpperCase() + string[i].slice(1); 
  }
  return string.join(' ');
}

function load() {
	var wrestler_link = document.getElementById("wrestler_link");
	wrestler_link.addEventListener("click", function () { change_entry("wrestler") });
}

document.addEventListener("DOMContentLoaded", load);