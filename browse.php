<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'connect.php';
	require 'authenticate.php';

	if(isset($_GET['type'])) {

		switch ($_GET['type']) {

			case 'match':
			    $query = "SELECT wrestlingmatch.wrestlingmatch_id, event_date, event.name, duration, finish, titles, type,
    				GROUP_CONCAT(DISTINCT CASE WHEN winner = 'y' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR ', ') AS 'winner',
    				GROUP_CONCAT(DISTINCT CASE WHEN winner = 'n' THEN wrestler.name ELSE NULL END ORDER BY winner DESC SEPARATOR ', ') AS 'loser'    	
    				FROM wrestlingmatch
    				JOIN event ON event.event_id = wrestlingmatch.event_id
    				JOIN matchwrestler ON matchwrestler.wrestlingmatch_id = wrestlingmatch.wrestlingmatch_id
    				JOIN wrestler ON wrestler.wrestler_id = matchwrestler.wrestler_id    
		    		GROUP BY matchwrestler.wrestlingmatch_id
			    	ORDER BY {$_GET['sort']}";

			    $statement = $db->prepare($query);
			    $statement->execute();
			    $entries = $statement->fetchAll();
				
			    break;
			default:
				$query = "SELECT * FROM {$_GET['type']} ORDER BY {$_GET['sort']}";
				$statement = $db->prepare($query);	
				$statement->execute();
				$entries = $statement->fetchAll();
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE - Browse Database</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Heebo:900&display=swap" rel="stylesheet"> 
</head>
<body>

	<div id="header">
		<img src="images/logo.png">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>	

	<ul id="menu">
		<li><a href="index.php">HOME</a></li>
		<li><a href="browse.php" class="active">BROWSE DATABASE</a></li>
		<li><a href="search.php">SEARCH DATABASE</a></li>
		<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
			<?php if ($_SESSION['user_type'] == 'admin'): ?>
				<li><a href="addEntry.php">ADD AN ENTRY</a></li>
			<?php endif ?>
		<?php endif ?>
	</ul>	

	<div id="content">
		<div id="side_menu">
			<h3>CATEGORY</h3>
			<ul>
				<li><a href="?type=wrestler&sort=wrestler_id">WRESTLERS</a></li>
				<li><a href="?type=match&sort=wrestlingmatch_id">MATCHES</a></li>
				<li><a href="?type=event&sort=event_id">EVENTS</a></li>
				<li><a href="?type=promotion&sort=promotion_id">PROMOTIONS</a></li>
			</ul>
		</div>


		<div class="wrapper">
			<?php if(isset($_GET['edit'])): ?>
				<h3><?= $_GET['edit'] ?> successful!</h3>
				<p>Please select a category to browse entries.</p>
			<?php else: ?>						

				<?php if(!isset($_GET['type'])): ?>
					<h3>BROWSE DATABASE</h3>
					<p>Please select a category to browse entries.</p>		
				<?php else: ?>
					<?php if ($entries == null): ?>
						<h3>Nothing found in database!</h3>
					<?php else: ?>

						<?php switch ($_GET['type']): 
							case 'wrestler': ?>
						
								<h3>BROWSE WRESTLERS</h3>
								<?php if($_GET['sort'] != 'wrestler_id'): ?>
									<h4>SORTED BY <?= strtoupper(str_replace('_', ' ', $_GET['sort'])); ?></h4>
								<?php endif ?>
								<table width="100%">				
									<thead>
										<tr>
											<th width="30%"><a href="browse.php?type=wrestler&sort=name">NAME</a></th>
											<th width="32%"><a href="browse.php?type=wrestler&sort=real_name">REAL NAME</a></th>
											<th><a href="browse.php?type=wrestler&sort=date_of_birth">DATE OF BIRTH</a></th>
											<th><a href="browse.php?type=wrestler&sort=nationality">NATIONALITY</a></th>
										</tr>						
									</thead>
									<tbody>
										<?php foreach($entries as $entry): ?>
											<tr onclick="location.href='show.php?type=wrestler&id=<?= $entry['wrestler_id'] ?>'">
												<td><?= $entry['name'] ?></td>
												<td><?= $entry['real_name'] ?></td>
												<td><?= $entry['date_of_birth'] ?></td>
												<td><?= $entry['nationality'] ?></td>
											</tr>		
										<?php endforeach ?>								
									</tbody>
								</table>
							<?php break; ?>

							<?php case 'match': ?>
								<h3>BROWSE MATCHES</h3>
								<?php if($_GET['sort'] != 'wrestlingmatch_id'): ?>
									<h4>SORTED BY: <?= strtoupper(str_replace('_', ' ', $_GET['sort'])); ?></h4>
								<?php endif ?>
								<table width="100%">				
									<thead>
										<tr>
											<th width="10%"><a href="browse.php?type=match&sort=event_date">DATE</a></th>
											<th><a href="browse.php?type=match&sort=name">EVENT NAME</a></th>
										</tr>						
									</thead>
									<tbody>
										<?php foreach($entries as $entry): ?>
										<tr onclick="location.href='show.php?type=wrestlingmatch&id=<?= $entry['wrestlingmatch_id'] ?>'">
											<td><?= $entry['event_date'] ?></td>
											<td><?= $entry['name'] ?></td>
											<td><?= $entry['winner'] ?></td>
											<td><?= $entry['finish'] ?></td>
											<td><?= $entry['loser'] ?></td>
										</tr>		
										<?php endforeach ?>								
									</tbody>
								</table>
								<?php break; ?>

							<?php case 'event': ?>
								<h3>BROWSE EVENTS</h3>
								<?php if($_GET['sort'] != 'event_id'): ?>
									<h4>SORTED BY: <?= strtoupper(str_replace('_', ' ', $_GET['sort'])); ?></h4>
								<?php endif ?>
								<table width="100%">				
									<thead>
										<tr>
											<th width="30%"><a href="browse.php?type=event&sort=name">NAME</a></th>
											<th width="10%"><a href="browse.php?type=event&sort=event_date">DATE</a></th>
											<th width="30%"><a href="browse.php?type=event&sort=venue">VENUE</a></th>
										</tr>						
									</thead>
									<tbody>
										<?php foreach($entries as $entry): ?>
										<tr onclick="location.href='show.php?type=event&id=<?= $entry['event_id'] ?>&sort=name'">
											<td><?= $entry['name'] ?></td>
											<td><?= $entry['event_date'] ?></td>
											<td><?= $entry['venue'] ?></td>
										</tr>			
										<?php endforeach ?>								
									</tbody>
								</table>
								<?php break; ?>

							<?php case 'promotion': ?>
								<h3>BROWSE PROMOTIONS</h3>
								<?php if($_GET['sort'] != 'promotion_id'): ?>
									<h4>SORTED BY: <?= strtoupper(str_replace('_', ' ', $_GET['sort'])); ?></h4>
								<?php endif ?>
								<table width="100%">				
									<thead>
										<tr>
											<th width="30%"><a href="browse.php?type=promotion&sort=name">NAME</a></th>
											<th width="30%"><a href="browse.php?type=promotion&sort=location">LOCATION</a></th>
										</tr>						
									</thead>
									<tbody>
										<?php foreach($entries as $entry): ?>
										<tr onclick="location.href='show.php?type=promotion&id=<?= $entry['promotion_id'] ?>&sort=name'">
											<td><?= $entry['name'] ?></td>
											<td><?= $entry['location'] ?></td>
										</tr>	
										<?php endforeach ?>								
									</tbody>
								</table>
								<?php break; ?>

						<?php endswitch; ?>
					<?php endif ?>
				<?php endif ?>	
			<?php endif ?>	
		</div>

		<?php if($_SESSION['loggedin'] == false): ?>		
			<div class="login">
				<h3>MEMBERS LOGIN</h3>
				<form method = "post" action="" id="login">
					<input type="text" name="username" placeholder="Username" required class="login_input"/>
					<input type="password" name="password" placeholder="Password" required class="login_input" />
					<input type="submit" value="LET ME IN!" class="login_input"/>
				</form>
				<a href="createAccount.php">Create An Account</a>	
			</div>
		<?php else: ?>
			<div class="account">
				<h3>ACCOUNT MENU</h3>
				<h4>Hi <?= $_SESSION['username'] ?>!</h4>
				<ul>
					<li><a href="logout.php">LOGOUT</a></li>
				</ul>
			</div>			
		<?php endif ?>		

	</div>	
</body>
</html>