<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'authenticate.php';
	require 'imageUpload.php';

	if(isset($_SESSION['loggedin']) && !$_SESSION['loggedin']) {
		header('Location: index.php');
	}

	if((isset($_SESSION['winner_count']) || isset($_SESSION['loser_count'])))
	{
		if(!isset($_GET['add_winner']) && !isset($_GET['add_loser'])) {
			unset($_SESSION['winner_count']);
			unset($_SESSION['loser_count']);
		}
	}

	if(isset($_POST['submit'])) {
		switch ($_GET['type']) {
			case 'wrestler':
			    $name  = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $real_name  = filter_input(INPUT_POST, 'real_name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $date_of_birth = $_POST['date_of_birth'];
			    $nationality  = filter_input(INPUT_POST, 'nationality', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $gender = $_POST['gender'];
			    $promotion  = $_POST['promotion'];
			    $query = "INSERT INTO wrestler (wrestler_id, name, real_name, date_of_birth, nationality, gender, promotion_id) 
			    			VALUES (NULL, :name, :real_name, :date_of_birth, :nationality, :gender, :promotion) ";
			    
			    $statement = $db->prepare($query);
			    $statement->bindValue(':name', $name);
			    $statement->bindValue(':real_name', $real_name);
			    $statement->bindValue(':date_of_birth', $date_of_birth);
			    $statement->bindValue(':nationality', $nationality);
			    $statement->bindValue(':gender', $gender);
			    $statement->bindValue(':promotion', $promotion);
			    
			    $successful = $statement->execute();

			    if($successful) {
			    	image_upload($db->lastInsertId());
			    }

				break;

			case 'match':
				$duration = ((int)$_POST['minutes'] * 60) + $_POST['seconds'];
				$finish = filter_input(INPUT_POST, 'finish', FILTER_SANITIZE_FULL_SPECIAL_CHARS);	
				$type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
				$titles = filter_input(INPUT_POST, 'titles', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
				$event_id = $_POST['event'];					

			    $match_query = "INSERT INTO wrestlingmatch (wrestlingmatch_id, duration, type, finish, titles, event_id) 
			    			VALUES (NULL, :duration, :type, :finish, :titles, :event_id)";

			    $statement = $db->prepare($match_query);
			    $statement->bindValue(':duration', $duration);
			    $statement->bindValue(':type', $type);
			    $statement->bindValue(':finish', $finish);
			    $statement->bindValue(':titles', $titles);
			    $statement->bindValue(':event_id', $event_id);		    
			    $successful = $statement->execute();	
			    $wrestlingmatch_id = $db->lastInsertId();

			    if($successful) {
			    	for ($i=1; $i <= $_SESSION['winner_count'] && $successful; $i++) { 
						$wrestler_id = $_POST['winner' . $i];
				    	$winner = 'y';
				    	$matchwrestler_query = "INSERT INTO matchwrestler (wrestler_id, wrestlingmatch_id, winner)
				    		VALUES (:wrestler_id, :wrestlingmatch_id, :winner)";
				    	$statement = $db->prepare($matchwrestler_query);
				    	$statement->bindValue(':wrestler_id', $wrestler_id);
				    	$statement->bindValue(':wrestlingmatch_id', $wrestlingmatch_id);
				    	$statement->bindValue(':winner', $winner);
				    	$successful =$statement->execute();
			    	}
				    

				    for ($i=1; $i <= $_SESSION['loser_count'] && $successful; $i++) { 
					    $wrestler_id = $_POST['loser' . $i];
					    $winner = 'n';
					    $matchwrestler_query = "INSERT INTO matchwrestler (wrestler_id, wrestlingmatch_id, winner)
					    	VALUES (:wrestler_id, :wrestlingmatch_id, :winner)";
					    $statement = $db->prepare($matchwrestler_query);
					    $statement->bindValue(':wrestler_id', $wrestler_id);
					    $statement->bindValue(':wrestlingmatch_id', $wrestlingmatch_id);
					    $statement->bindValue(':winner', $winner);
					    $successful = $statement->execute();
				    }			    	
			    }

				break;

			case 'event':
			    $name  = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $event_date = $_POST['event_date'];
			    $venue  = filter_input(INPUT_POST, 'venue', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $promotion  = $_POST['promotion'];
			    $query = "INSERT INTO event (event_id, name, event_date, venue, promotion_id) 
			    			VALUES (NULL, :name, :event_date, :venue, :promotion)";
			    
			    $statement = $db->prepare($query);
			    $statement->bindValue(':name', $name);
			    $statement->bindValue(':event_date', $event_date);
			    $statement->bindValue(':venue', $venue);
			    $statement->bindValue(':promotion', $promotion);
			    
			    $successful = $statement->execute();

				break;
			
			case 'promotion':
			    $name  = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $location  = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			    $query = "INSERT INTO promotion (promotion_id, name, location) 
			    			VALUES (NULL, :name, :location)";
			    
			    $statement = $db->prepare($query);
			    $statement->bindValue(':name', $name);
			    $statement->bindValue(':location', $location);
			    
			    $successful = $statement->execute();

				break;
		}
	}
	else {
	    if(isset($_GET['type'])) {
	    	switch ($_GET['type']) {
	    		case 'wrestler':
					$query = "SELECT * FROM promotion";
				    $statement = $db->prepare($query);
				    $statement->execute();
				    $promotions = $statement->fetchAll();		
	    		break;

	    		case 'match':

					if(!isset($_SESSION['winner_count'])) {
						$_SESSION['winner_count'] = 1;
					}

					if(!isset($_SESSION['loser_count'])) {
						$_SESSION['loser_count'] = 1;
					}	

	    			if(isset($_GET['add_winner'])) {
	    				$_SESSION['winner_count']++;
	    			}

					if(isset($_GET['add_loser'])) {
						$_SESSION['loser_count']++;
					}  			

	    			$query = "SELECT wrestler_id, name FROM wrestler ORDER BY name";
	   				$statement = $db->prepare($query);
	    			$statement->execute();
	    			$wrestlers = $statement->fetchAll();

					$query = "SELECT event_id, name FROM event";
				    $statement = $db->prepare($query);
				    $statement->execute();
				    $events = $statement->fetchAll();       			
	    		break;

	    		case 'event':
					$query = "SELECT promotion_id, name FROM promotion";
				    $statement = $db->prepare($query);
				    $statement->execute();
				    $promotions = $statement->fetchAll();
	    		break;
	    	} 
	    }		
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE - Add An Entry</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">	
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Heebo:900&display=swap" rel="stylesheet"> 
</head>
<body>
	<div id="header">
		<img src="images/logo.png">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>	

	<ul id="menu">
		<li><a href="index.php">HOME</a></li>
		<li><a href="browse.php">BROWSE DATABASE</a></li>
		<li><a href="search.php">SEARCH DATABASE</a></li>
		<li><a href="addEntry.php" class="active">ADD AN ENTRY</a></li>

	</ul>
			
	<form method="post" id="entry_form" enctype="multipart/form-data">
		<div id="side_menu">
			<h3>CATEGORY</h3>
			<ul>
				<li><a href="addEntry.php?type=wrestler">WRESTLER</a></li>
				<li><a href="addEntry.php?type=match">MATCH</a></li>
				<li><a href="addEntry.php?type=event">EVENT</a></li>
				<li><a href="addEntry.php?type=promotion">PROMOTION</a></li>						
			</ul>
		</div>
			
		<?php if (isset($successful)): ?>
			<?php if($successful): ?>
				<div class="wrapper">
					<h3>Successfully added 
						<?php echo $_GET['type'] === 'event' ? 'an '.$_GET['type'] : 'a '.$_GET['type'] ?>!</h3>
					<p>Please select a category to add another entry.</p>
				</div>
			<?php else: ?>
				<div class="wrapper">
					<h3>Something went wrong...</h3>
					<p>Please try again.</p>
				</div>
			<?php endif ?>	
		
		<?php else: ?>	
			<?php if(isset($_GET['type'])): ?>
				<?php if($_GET['type'] === 'wrestler'): ?>
					<div class="wrapper">
						<h3>ADD A NEW WRESTLER</h3>
						<label for="name">NAME</label>
						<input type="text" name="name" class="info" required>

						<!-- value="<?php echo isset($_POST['name']) ? $_POST['name'] : '' ?>"  -->

						<label for="real_name">REAL NAME</label>
						<input type="text" name="real_name" class="info" required>

						<label for="date_of_birth">DATE OF BIRTH</label>
						<input type="date" name="date_of_birth" id="date_of_birth" required>

						<label for="nationality">NATIONALITY</label>
						<input type="text" name="nationality" class="info" required>

						<label for="gender">GENDER</label>
						<select name="gender" required >
							<option value="">Please select...</option>
							<option value="m">Male</option>
							<option value="f">Female</option>
							<option value="o">Other</option>
						</select>

						<label for="promotion">PROMOTION</label>
						<select name="promotion" required >
							<option value="">Please select...</option>				
							<?php foreach($promotions as $promotion): ?>
								<option value="<?=$promotion['promotion_id'] ?>"><?=$promotion['name'] ?></option>
							<?php endforeach ?>
						</select>					

						<h3>ADD AN IMAGE</h3>
						<label for="image">ENTER IMAGE FILENAME:</label>
						<input type="file" name="image" id="image">

						<input type="submit" name="submit" value="SUBMIT" class="submitbutton">
					</div>
				<?php endif ?>

				<?php if($_GET['type'] === 'match'): ?>
					<div class="wrapper">
						<h3>ADD A NEW MATCH</h3>

						<label for="participants">PARTICIPANTS</label>
						<div id="participants">
							<div>					
								<?php for ($i=1; $i <= $_SESSION['winner_count']; $i++): ?>
									<select name="winner<?= $i ?>" required >
										<option value="">Winner</option>			
										<?php foreach($wrestlers as $wrestler): ?>
											<option value="<?=$wrestler['wrestler_id']?>"><?=$wrestler['name'] ?></option>
										<?php endforeach ?>
									</select>
								<?php endfor ?>
								<small><a href="addEntry.php?type=match&add_winner=true">Add Winner</a></small>	
							</div>
						
							<select name="finish" required >
								<option value="">Result</option>
								<option value="Def. (Pin)">Defeated (via Pinfall)</option>
								<option value="Def. (Submission)">Defeated (via Submission)</option>
								<option value="Def. (DQ)">Defeated (via Disqualification)</option>
								<option value="No Contest">No Contest</option>											
							</select>

							<div>					
								<?php for ($i=1; $i <= $_SESSION['loser_count']; $i++): ?>
									<select name="loser<?= $i ?>" required >
										<option value="">Loser</option>			
										<?php foreach($wrestlers as $wrestler): ?>
											<option value="<?=$wrestler['wrestler_id']?>"><?=$wrestler['name'] ?></option>
										<?php endforeach ?>
									</select>
								<?php endfor ?>
								<small><a href="addEntry.php?type=match&add_loser=true">Add Loser</a></small>	
							</div>

						</div>				

						<label for="duration">DURATION</label>
						<div id="duration">
							<label for="minutes">MINUTES:</label>					
							<input type="number" name="minutes" min="0" value="0" class="duration" required>
							<label for="seconds">SECONDS:</label>
							<input type="number" name="seconds" min="0" max="59"  value="0" class="duration" required>
						</div>

						<label for="type">TYPE</label>
						<input type="text" name="type" class="info">

						<label for="titles">TITLES</label>
						<input type="text" name="titles" class="info">													

						<label for="event">EVENT</label>
						<select name="event" required >
							<option value="">Please select...</option>
							<option value="none">None</option>						
							<?php foreach($events as $event): ?>
								<option value="<?=$event['event_id'] ?>"><?=$event['name'] ?></option>
							<?php endforeach ?>
						</select>

						<input type="submit" name="submit" value="SUBMIT" class="submitbutton">
					</div>
				<?php endif ?>

				<?php if($_GET['type'] === 'event'): ?>
					<div class="wrapper">
						<h3>ADD A NEW EVENT</h3>
						<label for="name">NAME</label>
						<input type="text" name="name" class="info" required>

						<label for="event_date">DATE</label>
						<input type="date" name="event_date" id="event_date" required>

						<label for="venue">VENUE</label>
						<input type="text" name="venue" class="info" required>					

						<label for="promotion">PROMOTION</label>
						<select name="promotion" required >
							<option value="">Please select...</option>
							<option value="none">None</option>						
							<?php foreach($promotions as $promotion): ?>
								<option value="<?=$promotion['promotion_id'] ?>"><?=$promotion['name'] ?></option>
							<?php endforeach ?>
						</select>

						<input type="submit" name="submit" value="SUBMIT" class="submitbutton">
					</div>
				<?php endif ?>

				<?php if($_GET['type'] === 'promotion'): ?>
					<div class="wrapper">
						<h3>ADD A NEW PROMOTION</h3>
						<label for="name">NAME</label>
						<input type="text" name="name" class="info" required>

						<label for="name">LOCATION</label>
						<input type="text" name="location" class="info" required >

						<input type="submit" name="submit" value="SUBMIT" class="submitbutton">
					</div>
				<?php endif ?>		
			<?php else: ?>
				<div class="wrapper">
					<h3>ADD AN ENTRY</h3>
					<p>Please select a category to add an entry.</p>
				</div>
					
			<?php endif ?>
		<?php endif ?>	 
		<div class="account">
			<h3>ACCOUNT MENU</h3>
			<h4>Hi <?= $_SESSION['username'] ?>!</h4>
			<ul>
				<li><a href="logout.php">LOGOUT</a></li>
			</ul>
		</div>			
	</form>	
</body>
</html>