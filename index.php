<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'connect.php';
	require 'authenticate.php';

    $query = "SELECT rating.wrestlingmatch_id, event_date, ROUND(AVG(star_rating),2) AS avg_rating,
    	GROUP_CONCAT(DISTINCT CASE WHEN winner = 'y' THEN wrestler.name ELSE NULL END ORDER BY winner DESC) AS 'winners',
    	GROUP_CONCAT(DISTINCT CASE WHEN winner = 'n' THEN wrestler.name ELSE NULL END ORDER BY winner DESC) AS 'losers'
    	FROM rating
    	JOIN wrestlingmatch ON rating.wrestlingmatch_id = wrestlingmatch.wrestlingmatch_id
    	JOIN event ON event.event_id = wrestlingmatch.event_id
    	JOIN matchwrestler ON matchwrestler.wrestlingmatch_id = wrestlingmatch.wrestlingmatch_id
    	JOIN wrestler ON wrestler.wrestler_id = matchwrestler.wrestler_id
    	GROUP BY matchwrestler.wrestlingmatch_id
    	ORDER BY avg_rating DESC LIMIT 5";

    $statement = $db->prepare($query);
    $statement->execute();
    $top_matches = $statement->fetchAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Heebo:900&display=swap" rel="stylesheet"> 
</head>
<body>
	
	<div id="header">
		<img src="images/logo.png">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>	

	<ul id="menu">
		<li><a href="index.php" class="active">HOME</a></li>
		<li><a href="browse.php">BROWSE DATABASE</a></li>
		<li><a href="search.php">SEARCH DATABASE</a></li>
		<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
			<?php if ($_SESSION['user_type'] == 'admin'): ?>
				<li><a href="addEntry.php">ADD AN ENTRY</a></li>
			<?php endif ?>
		<?php endif ?>
	</ul>

	<div id="content">
		<div class="wrapper">
			<h3>NEWS</h3>
		</div>

		<div class="wrapper">
			<h3>TOP RATED MATCHES</h3>
			<div class="datatable">				
				<table width="100%">				
					<thead>
						<tr>
							<th>DATE</th>
							<th>RATING</th>
						</tr>						
					</thead>
					<tbody>
						<?php foreach($top_matches as $match): ?>
							<tr onclick="location.href='show.php?type=wrestlingmatch&id=<?= $match['wrestlingmatch_id'] ?>'">
								<td><?= $match['event_date'] ?></td>
								<td><?= $match['avg_rating'] ?></td>
								<td><?= $match['winners'] ?></td>
								<td>VS</td>
								<td><?= $match['losers'] ?></td>
							</tr>		
						<?php endforeach ?>								
					</tbody>
				</table>
			</div>		
		</div>

		<?php if($_SESSION['loggedin'] == false): ?>		
			<div class="login">
				<h3>MEMBERS LOGIN</h3>
				<form method = "post" action="" id="login">
					<input type="text" name="username" placeholder="Username" required class="login_input"/>
					<input type="password" name="password" placeholder="Password" required class="login_input" />
					<input type="submit" value="LET ME IN!" class="login_input"/>
				</form>
				<a href="createAccount.php">Create An Account</a>	
			</div>
		<?php else: ?>
			<div class="account">
				<h3>ACCOUNT MENU</h3>
				<h4>Hi <?= $_SESSION['username'] ?>!</h4>
				<ul>
					<li><a href="logout.php">LOGOUT</a></li>
				</ul>
			</div>			
		<?php endif ?>						
	</div>
</body>
</html>