<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'authenticate.php';
	require 'connect.php';

	$name_valid = true;
	$real_name_valid = true;
	$date_of_birth_valid = true;
	$nationality_valid = true;

	if(isset($_POST['submit'])) {
		if(strlen($_POST['name']) < 1) {
			$name_valid = false;
		}
		if(strlen($_POST['real_name']) < 1) {
			$real_name_valid = false;
		}
		if(strlen($_POST['date_of_birth']) < 1) {
			$date_of_birth_valid = false;
		}
		if(strlen($_POST['nationality']) < 1) {
			$nationality_valid = false;
		}

		if($name_valid && $real_name_valid && $date_of_birth_valid && $nationality_valid) {
		    $name  = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		    $real_name  = filter_input(INPUT_POST, 'real_name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		    $date_of_birth = $_POST['date_of_birth'];
		    $nationality  = filter_input(INPUT_POST, 'nationality', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
		    $gender = $_POST['gender'];
		    //$promotion  = 
		    $query = "INSERT INTO wrestler (wrestler_id, name, real_name, date_of_birth, nationality, gender) 
		    			VALUES (NULL, :name, :real_name, :date_of_birth, :nationality, :gender) ";
		    
		    $statement = $db->prepare($query);
		    $statement->bindValue(':name', $name);
		    $statement->bindValue(':real_name', $real_name);
		    $statement->bindValue(':date_of_birth', $date_of_birth);
		    $statement->bindValue(':nationality', $nationality);
		    $statement->bindValue(':gender', $gender);
		    
		    $statement->execute();

		    header("Location: index.php");
		    exit();
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE - Add Wrestler</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">	
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet"> 
</head>
<body>
	<div id="header">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>			
	</div>

	<ul id="menu">
		<li><a href="index.php">HOME</a></li>
		<li><a href="browseWrestlers.php">BROWSE WRESTLERS</a></li>
		<li><a href="addWrestler.php" class="active">ADD WRESTLER</a></li>
		<li><a href="">LOGIN</a></li>
	</ul>
	<div id="content">
		<div class="wrapper">
			<h3>Add New Wrestler</h3>		
			<div id="all_entries">
			<fieldset>
				<form method="post">
					<label for="name">Name</label>
					<input type="text" name="name" id="name" value="<?php echo isset($_POST['name']) ? $_POST['name'] : '' ?>" >
					<?php if(!$name_valid): ?>
						<p id="error">Name Cannot Be Blank!</p>
					<?php endif ?>

					<label for=real_name>Real Name</label>
					<input type="text" name="real_name" id="real_name" value="<?php echo isset($_POST['real_name']) ? $_POST['real_name'] : '' ?>" >
					<?php if(!$real_name_valid): ?>
						<p id="error">Real Name Cannot Be Blank!</p>
					<?php endif ?>

					<label for=real_name>Date Of Birth</label>
					<input type="date" name="date_of_birth" id="date_of_birth">
					<?php if(!$date_of_birth_valid): ?>
						<p id="error">Select a valid date of birth!</p>
					<?php endif ?>				

					<label for=real_name>Nationality</label>
					<input type="text" name="nationality" id="nationality" value="<?php echo isset($_POST['nationality']) ? $_POST['nationality'] : '' ?>" >
					<?php if(!$nationality_valid): ?>
						<p id="error">Nationality Cannot Be Blank!</p>
					<?php endif ?>

					<label for=real_name>Gender</label>
					<select name="gender" id="gender">
						<option value="m">Male</option>
						<option value="f">Female</option>
						<option value="o">Other</option>
					</select>	
					
					<input type="submit" name="submit" value="Submit" class="submitbutton">
				</form>
			</fieldset>
			</div>
		</div>
	</div>
</body>
</html>