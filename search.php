<!--

    Web Dev 2 Project
    Name: Omar Ducut
    Date: -
    Description: -

-->

<?php
	require 'connect.php';
	require 'authenticate.php';
	
	if(isset($_GET['type'])) {

		switch ($_GET['type']) {
			case 'wrestler':
				$query = "SELECT promotion_id, name FROM promotion";
			    $statement = $db->prepare($query);
			    $statement->execute();
			    $promotions = $statement->fetchAll();	
				break;
			default:
				break;	
		}
	}

	if(isset($_POST['submit'])) {

		switch ($_GET['type']) {
			case 'wrestler':
				$name  = '%' . filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS) . '%';
				$real_name  = '%' . filter_input(INPUT_POST, 'real_name', FILTER_SANITIZE_FULL_SPECIAL_CHARS) . '%';
				$date_of_birth = $_POST['date_of_birth'];
			    $nationality  = '%' . filter_input(INPUT_POST, 'nationality', FILTER_SANITIZE_FULL_SPECIAL_CHARS) . '%';
			    $gender = '%' . $_POST['gender'] . '%';
			    $promotion_id  = $_POST['promotion'];		

				$query = "SELECT * FROM {$_GET['type']} 
					WHERE name LIKE :name
					AND real_name LIKE :real_name
					AND ((:date_of_birth = '') OR (date_of_birth = :date_of_birth))
					AND nationality LIKE :nationality
					AND gender LIKE :gender
					AND((:promotion_id = '') OR (promotion_id = :promotion_id))";
				$statement = $db->prepare($query);
			    $statement->bindValue(':name', $name);
			    $statement->bindValue(':real_name', $real_name);
			    $statement->bindValue(':date_of_birth', $date_of_birth);
			    $statement->bindValue(':nationality', $nationality);
			    $statement->bindValue(':gender', $gender);
			    $statement->bindValue(':promotion_id', $promotion_id);
				$statement->execute();
				$entries = $statement->fetchAll();
				break;
			default:
				break;	
		}

	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>ProGraps DATABASE - Search Database</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Heebo:900&display=swap" rel="stylesheet"> 
</head>
<body>
	
	<div id="header">
		<img src="images/logo.png">
		<h1><a href="index.php">ProGraps DATABASE</a></h1>
	</div>	

	<ul id="menu">
		<li><a href="index.php">HOME</a></li>
		<li><a href="browse.php">BROWSE DATABASE</a></li>
		<li><a href="search.php" class="active">SEARCH DATABASE</a></li>
		<?php if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true): ?>
			<?php if ($_SESSION['user_type'] == 'admin'): ?>
				<li><a href="addEntry.php">ADD AN ENTRY</a></li>
			<?php endif ?>
		<?php endif ?>
	</ul>

	<form method="post" id="entry_form">
		<div id="side_menu">
			<h3>CATEGORY</h3>
			<ul>
				<li><a href="?type=wrestler">WRESTLERS</a></li>
				<li><a href="?type=match">MATCHES</a></li>
				<li><a href="?type=event">EVENTS</a></li>
				<li><a href="?type=promotion">PROMOTIONS</a></li>
			</ul>
		</div>

		<?php if(!isset($_GET['type'])): ?>
			<div class="wrapper">
				<h3>SEARCH FOR AN ENTRY</h3>
				<p>Please select a category to search entries.</p>
			</div>
		<?php else: ?>
			<div class="wrapper">
				<?php if(isset($_POST['submit'])): ?>
					<?php if(count($entries) === 0): ?>
						<h3>NO RESULTS FOUND!</h3>
						<p>Please refine your search.</p>
					<?php else: ?>	
						<h3>SEARCH RESULTS</h3>
						<table width="100%">				
							<thead>
								<tr>
									<th width="30%"><a href="">NAME</a></th>
									<th width="32%"><a href="">REAL NAME</a></th>
									<th><a href="">DATE OF BIRTH</a></th>
									<th><a href="">NATIONALITY</a></th>
								</tr>						
							</thead>
							<tbody>
								<?php foreach($entries as $entry): ?>
									<tr onclick="location.href='show.php?type=wrestler&id=<?= $entry['wrestler_id'] ?>'">
										<td><?= $entry['name'] ?></td>
										<td><?= $entry['real_name'] ?></td>
										<td><?= $entry['date_of_birth'] ?></td>
										<td><?= $entry['nationality'] ?></td>
									</tr>		
								<?php endforeach ?>								
							</tbody>
						</table>
						</br>
					<?php endif ?>			
				<?php endif ?>

				<?php switch ($_GET['type']): 
					case 'wrestler': ?>
						<h3>SEARCH FOR A WRESTLER</h3>
						
						<label for="criteria" ><h4>SEARCH BY:</h4></label>
						<label for="name">NAME</label>
						<input type="text" name="name" class="info">

						<label for="real_name">REAL NAME</label>
						<input type="text" name="real_name" class="info">

						<label for="date_of_birth">DATE OF BIRTH</label>
						<input type="date" name="date_of_birth" id="date_of_birth">

						<label for="nationality">NATIONALITY</label>
						<input type="text" name="nationality" class="info">

						<label for="gender">GENDER</label>
						<select name="gender">
							<option value="">Please select...</option>
							<option value="m">Male</option>
							<option value="f">Female</option>
							<option value="o">Other</option>
						</select>

						<label for="promotion">PROMOTION</label>
						<select name="promotion">
							<option value="">Please select...</option>			
							<?php foreach($promotions as $promotion): ?>
								<option value="<?=$promotion['promotion_id'] ?>"><?=$promotion['name'] ?></option>
							<?php endforeach ?>
						</select>		
						<input type="submit" name="submit" value="SEARCH" class="submitbutton">
					<?php break; ?>
			
					<?php case 'match': ?>
						<h3>SEARCH FOR A MATCH</h3>
						
						<label for="criteria" >SEARCH BY:</label>
						<div id="search_criteria">
							<input type="radio" name="criteria" value="name">
							<label for="name"><h4>Name</h4></label>
							<input type="radio" name="criteria" value="real_name">
							<label for="real_name"><h4>Real Name</h4></label>
							<input type="radio" name="criteria" value="date_of_birth">
							<label for="date_of_birth"><h4>Date of Birth</h4></label>
							<input type="radio" name="criteria" value="nationality">
							<label for="nationality"><h4>Nationality</h4></label>
							<input type="radio" name="criteria" value="gender">
							<label for="gender"><h4>Gender</h4></label>
						</div>			
						<input type="input" name="submit" value="SEARCH">
					<?php break; ?>

				<?php endswitch; ?>
			</div>	
		<?php endif ?>		

		<?php if($_SESSION['loggedin'] == false): ?>		
			<div class="login">
				<h3>MEMBERS LOGIN</h3>
				<form method = "post" action="" id="login">
					<input type="text" name="username" placeholder="Username" required class="login_input"/>
					<input type="password" name="password" placeholder="Password" required class="login_input" />
					<input type="submit" value="LET ME IN!" class="login_input"/>
				</form>
				<a href="createAccount.php">Create An Account</a>	
			</div>
		<?php else: ?>
			<div class="account">
				<h3>ACCOUNT MENU</h3>
				<h4>Hi <?= $_SESSION['username'] ?>!</h4>
				<ul>
					<li><a href="logout.php">LOGOUT</a></li>
				</ul>
			</div>			
		<?php endif ?>	

	</form>	

				
</body>
</html>